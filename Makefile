# =================================== GitOps Demo ===================================

# ----------------------- SETUP -----------------------

setup:
	@echo "== Installing GitOps Flux Toolkit =="
	@./scripts/gitops-setup.sh $(GITLAB_TOKEN) $(GITLAB_PROJECT_ID)

clean-up:
	@echo "== cleaning previous run if any =="
	@./scripts/gitops-cleanup.sh

install: clean-up setup

