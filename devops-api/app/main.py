from typing import Union
from fastapi import FastAPI

app = FastAPI()

@app.get("/health")
async def health():
    return {"message": "Hellow from a tree hugger"}

@app.get("/")
async def root():
    return {"message": "Hola DevOpsDay 2023!!!"}
