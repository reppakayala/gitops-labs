# GitOps Labs

## Requirements

- Flux CLI
- Kind
- Docker
- Git

## How to run:

Fork this repo, and clone, then run:

```
make install GITLAB_TOKEN=${GITLAB_TOKEN} GITLAB_PROJECT_ID=${GITLAB_PROJECT_ID}
```

## How to destroy
Just:
```
make clean-up
```

Make sure to remove the deploy key from the repo.


### Project:

```
.
├── Makefile
├── README.md
├── clusters
│   └── nonprod
│       └── namespaces
│           ├── app
│           │   └── dev
│           │       ├── devops-api
│           │       │   └── deployment.yaml
│           │       └── namespace.yaml
│           └── k8s
│               └── flux-system
├── devops-api
│   ├── Dockerfile
│   ├── app
│   │   ├── __init__.py
│   │   └── main.py
│   ├── poetry.lock
│   ├── pyproject.toml
│   └── requirements.txt
├── known_hosts
└── scripts
    ├── gitops-cleanup.sh
    └── gitops-setup.sh

11 directories, 13 files
```

## Authors

Created by:

- David Caballero [Gitlab](https://gitlab.com/arkhoss) | [Github](https://github.com/arkhoss) | d@dcaballero.net

