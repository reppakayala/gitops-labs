#!/bin/bash

AWS_PROFILE=gitopsdemo
GITLAB_TOKEN=$1
GITLAB_PROJECT_ID=$2

kind create cluster --config=<<EOF
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  extraPortMappings:
  - containerPort: 5000
    hostPort: 5000
    protocol: tcp
- role: worker
EOF

kubectl cluster-info --context kind-kind

flux install \
--export \
--network-policy=true \
--watch-all-namespaces=true \
--namespace=flux-system \
--components-extra=image-reflector-controller,image-automation-controller \
> clusters/nonprod/namespaces/k8s/flux-system/gotk-components.yaml

kubectl apply -f clusters/nonprod/namespaces/k8s/flux-system/gotk-components.yaml

mkdir -p clusters/nonprod/namespaces/k8s/flux-system/sources

flux create source git gitops-labs \
-n flux-system \
--export \
--url=ssh://git@gitlab.com/arkhoss/gitops-labs \
--branch=master \
--interval=1m \
--secret-ref=gitops-labs-ssh-credentials \
> clusters/nonprod/namespaces/k8s/flux-system/sources/gitops-labs.yaml

ssh-keygen -q -t rsa -b 4096 -C "d@dcaballero.net" -N "" -f ./identity
ssh-keyscan gitlab.com > ./known_hosts

kubectl create secret generic gitops-labs-ssh-credentials \
-n flux-system \
--dry-run=client \
--from-file=./identity \
--from-file=./identity.pub \
--from-file=./known_hosts \
-o yaml \
> clusters/nonprod/namespaces/k8s/flux-system/sources/gitops-labs-ssh-credentials.decrypted.yaml

PUB_KEY=$(cat ./identity.pub)

deploy_key_data()
{
   cat <<EOF
{
    "title": "gitops-labs", 
    "key": "${PUB_KEY}", 
    "can_push": "true"
}
EOF
}

curl --silent --request POST \
  --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
  --header "Content-Type: application/json" \
  --data "$(deploy_key_data)" \
     "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/deploy_keys/" | jq -r .created_at

rm -rf ./identity*

kubectl apply -f clusters/nonprod/namespaces/k8s/flux-system/sources/gitops-labs-ssh-credentials.decrypted.yaml
kubectl apply -f clusters/nonprod/namespaces/k8s/flux-system/sources/gitops-labs.yaml

mkdir -p clusters/nonprod/namespaces/k8s/flux-system/kustomizations
mkdir -p clusters/nonprod/namespaces/k8s/flux-system/imageautomation

pwd

git add .

git commit -m "cluster boostrap flux and secret"

git push origin master

flux create kustomization gitops-labs \
-n flux-system \
--source=GitRepository/gitops-labs \
--path="/clusters/nonprod" \
--prune=true \
--interval=1m \
--decryption-provider=sops \
--export \
> clusters/nonprod/namespaces/k8s/flux-system/kustomizations/gitops-labs.yaml

kubectl apply -f clusters/nonprod/namespaces/k8s/flux-system/kustomizations/gitops-labs.yaml

flux create image repository devops-api \
-n flux-system \
--image arkhoss/devops-api \
--interval 1m \
--export \
> clusters/nonprod/namespaces/k8s/flux-system/imageautomation/image-repositories.yaml 

kubectl apply -f clusters/nonprod/namespaces/k8s/flux-system/imageautomation/image-repositories.yaml

flux create image policy devops-api-dev \
-n flux-system \
--image-ref=devops-api \
--select-alpha=asc \
--filter-regex='^dev-(?P<ts>[0-9]{8}-[0-9]{6})_.+$' \
--filter-extract='$ts' \
--export \
> clusters/nonprod/namespaces/k8s/flux-system/imageautomation/image-policies-dev.yaml 

kubectl apply -f clusters/nonprod/namespaces/k8s/flux-system/imageautomation/image-policies-dev.yaml  


flux create image update gitops-labs \
--namespace=flux-system \
--git-repo-ref=gitops-labs \
--git-repo-namespace=flux-system \
--git-repo-path="./clusters/nonprod" \
--checkout-branch=master \
--author-name=flux \
--author-email=flux@example.com \
--commit-template="{{range .Updated.Images}}{{println .}}{{end}}" \
--export \
> clusters/nonprod/namespaces/k8s/flux-system/imageautomation/image-update-automation.yaml

kubectl apply -f clusters/nonprod/namespaces/k8s/flux-system/imageautomation/image-update-automation.yaml

